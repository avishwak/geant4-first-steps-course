**Solutions for Task B3
---------------------**


Exercise 1
----------

- a. 
  /gun/particle e-
  /gun/particle e+
  /gun/particle proton
  /gun/particle gamma
  /gun/particle mu-

- b. 
  /gun/energy 30 MeV

- c. 
  /tracking/verbose 1  
  /run/beamOn 1

Exercise 2.
-----------

- a.
    G4ParticleDefinition* part = G4ParticleTable::GetParticleTable()->FindParticle( "e-" );
    fParticleGun->SetParticleDefinition( part );

- b.
    fParticleGun->SetParticleEnergy( 10.*CLHEP::MeV );

- c.
    fParticleGun->SetParticlePosition( G4ThreeVector(-5*mm,0.,0.) );

- d.
    fParticleGun->SetParticleMomentumDirection( G4ThreeVector(1.,0.,0.) );

To check how many times SetDefaultKinematics is called in the application, you can add an output line inside it such as
```
   G4cout << "SetDefaultKinematics called.  Set particleGun's particle properties: " << G4endl;
   G4cout << "     particle = " << fParticleGun->GetParticleDefinition()->GetParticleName() << G4endl;
   //  Look at the file $G4INCLUDE/G4ParticleDefinition.hh to find 
   //      G4ParticleDefinition's other methods
   G4cout << "     position = " << fParticleGun->GetParticlePosition() << G4endl;
   G4cout << "     direction= " << fParticleGun->GetParticleMomentumDirection() << G4endl;
```
and search the output to see how many times and where it is called.

Exercise 3
----------

Change primary particle parameters in YourPrimaryGenerator class
- a. define primary e+
- b. define position on the surface of the target
- c. define random position on the surface for each new primary

- a.
    G4ParticleDefinition* part = G4ParticleTable::GetParticleTable()->FindParticle( "e+" );
    fParticleGun->SetParticleDefinition( part );

- b.
    fParticleGun->SetParticlePosition( G4ThreeVector(-1.*cm,0.,0.) );

- c.
    G4double x = - 1. * cm;
    G4double y = (-1.25 + 2.5*G4UniformRand()) * cm;
    G4double z = (-1.25 + 2.5*G4UniformRand()) * cm;
    fParticleGun->SetParticlePosition( G4ThreeVector( x, y, z) );


To check when and how many times the GeneratePrimaries() method is called in the application, you can add an output line inside it such as
```
   G4cout << "YourPrimaryGeneratorAction::GeneratePrimaries called.  " << G4endl;
   G4cout << "  It's particleGun will create a particle of type, energy, position and momentum direction: " << G4endl;
   G4cout << "     particle = " << fParticleGun->GetParticleDefinition()->GetParticleName() << G4endl;
   //  Look at the file $G4INCLUDE/G4ParticleDefinition.hh to find G4ParticleDefinition's other methods
   G4cout << "     position = " << fParticleGun->GetParticlePosition() << G4endl;
   G4cout << "     direction= " << fParticleGun->GetParticleMomentumDirection() << G4endl;
   G4cout << "     kinetic E= " << fParticleGun->GetParticleEnergy() << G4endl;
   //  Look at the file $G4INCLUDE/G4ParticleGun.hh for G4ParticleGun's other methods
```
and search the output to see how many times and where it is called.
