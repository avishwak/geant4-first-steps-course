
#ifndef YOURPRIMARYGENERATORACTION_HH
#define YOURPRIMARYGENERATORACTION_HH

#include "G4VUserPrimaryGeneratorAction.hh"

#include "globals.hh"

// forward declaration
class YourDetectorConstruction;
class G4ParticleGun;
class G4Event;
class G4String;

class YourPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  // Method declaration:
  public:

    // CTR & DTR:
    // NOTE: the primary gun x-position depends on the target thickness so we
    //       need to be able to reach the detector to get this information
    YourPrimaryGeneratorAction(YourDetectorConstruction* det);
    virtual ~YourPrimaryGeneratorAction();

    // (Pure) virtual method to generata primary events
    virtual void GeneratePrimaries(G4Event*);

    //
    // Additional custom methods:

    // Public method to set the default primary particle kinematics
    void            SetDefaultKinematic();


  // Data member declarations:
  private:

    YourDetectorConstruction* fYourDetector;

    G4ParticleGun* fParticleGun;
};

#endif
