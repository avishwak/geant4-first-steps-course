UI Commands and Messengers: Hands-on
======================================

Goal
------

The goal of this exercise is to get familiar with the tools provided by `Geant4` to make your simulation application flexible regarding its settings.


Starting point
---------------
The starting point of the exercise is the simple `Geant4` application provided under the `ExDay4-D1` directory. It consists of a simple slab as detector/target that is hit by monoenergetic particles. The task is to extend this base application by adding the functionality of setting the target material through a custom UI command. In order to keep this simple, only the pre-defined `Geant4` NIST materials will be considered
that can be constructed by their unique name.

Step-by-step
--------------

1. Build and execute the provided base application. The application can be executed in:
   - ***batch*** mode by using the provided `g4Macro.mac` `Geant4` macro as   
   ```
   ./yourApplication ../g4Macro.mac
   ```
   - or in ***interactive*** mode as
   ```
   ./yourApplication
   ```
   - when executing the application in *interactive* mode, one can execute the provided `vis.mac` visualisation macro **from the GUI session** as
   ```
   /run/initialise
   /control/execute ../vis.mac
   ```
   (after the initialisation of the run).

   - make sure that you can identify the information regarding the target  material in the printout (batch mode) or execute the
   ```
   /cuts/dump
   ```
   command in the GUI session (interactive mode) to get this information.
2. By inspecting the provided `YourDetectorConstruction` class, make sure that you understand how the detector material is specified (compare this to `ExDay1` `Task-A2`). Try to modify the target material by hand, e.g. setting it to `G4_WATER`. Verify that your change of target material was indeed achieved. Note, that you need to rebuild the application in case of any changes in the code.


3. Now we will start to extend the base application in order to be able to change the target material from UI command. The first step is to create `YourDetectorMessenger` class:
   - a `G4UImessenger` to encapsulate **UI commands** related to the your detector. Such a UI command, that we want to implement now, is for changing the detector material. Understand why the pointer to your detector object is passed in the constructor!

   - since materials can be identified by their name, the material (name: `G4String`) name will be the parameter of our command:
     - add the `G4UIcmdWithAString* fTargetMaterialCMD` declaration into `YourDetectorMessenger` header
     - implement the construction of this command in the constructor of `YourDetectorMessenger`

   - implement the `virtual void SetNewValue(G4UIcommand*, G4String)` interface with this single command
     - you will need to identify that the your `fTargetMaterialCMD` command was invoked and use the `fYourDetector` pointer to invoke the required method of `YourDetectorConstruction` to set the target material

4. Add a `YourDetectorMessenger` member to `YourDetectorConstruction` in order to link them:
   - make sure you understand why we use only **forward declaration** in `YourDetectorConstruction.hh` and `#include` only in `YourDetectorConstruction.cc`
   - create `YourDetectorMessenger` object in the CTR of `YourDetectorConstruction` and delete in the DTR

5. Build your modified application.

6. Add a command to the `g4Macro.mac` to change the target material to `G4_WATER`:
   - make sure you understand why you need to place the
     ```
     /yourApp/det/setTargetMaterial G4_WATER
     ```
     command before
     ```
     /run/initialize
     ```
     in the macro file
   - verify that your detector material has indeed been changed  

7. Run the application in interactive mode:
   - can you see your own command now in helper menu of the GUI (on the left)?
   - use your command to set the target material to `G4_Pb`,
     initialise the run and execute the visualisation macro then simulate 100 primaries (with the default type of electron and kinetic energy of 30 [MeV]). From your GUI session:   
     ```
     /yourApp/det/setTargetMaterial G4_Pb
     /run/initialize
     /control/execute ../vis.mac
     /run/beamOn 100
     ```
    - change the target material now to `G4_WATER` by using your command. Run the same simulation. What you expect to see compared to lead? What you see? What's going on :flushed:?

      You might find the
        ```
        /run/reinitializeGeometry true
        ```
      command useful, especially invoking it after you changed your detector material :wink:    
