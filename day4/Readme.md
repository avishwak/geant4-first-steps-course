Day 4 topics
============

Initial items
-------------
- Review homework & answer questions
- Follow-up concepts from homework - extended concepts
- Reset set-up of hands-on

New topics
----------
- Messengers
- EM Physics
- Hadronic Physics

Hands-on
--------
- Task [D1](./Task-D1.md) Messenger - how to create one
- Task [D2](./Task-D2.md) EM physics exercises
- Task [D3](./Task-D3.md) Hadronic: choose hadronic physics list, observing hadron showers
     + compare profile of pion shower to electron shower

Homework (proposal for standard template)
--------
- Continue one of the aspects
- Study an extended example
Task [DH1](Task-DH1.md)
Task [DH2](Task-DH2.md)
Task [DH3](Task-DH3.md)

Developers:
----------
Messengers: Mihaly
EM Physics: Mihaly/Vladimir
Hadronic Physics: Alberto
