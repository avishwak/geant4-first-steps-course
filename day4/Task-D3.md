

-  Try to shoot a hadron (e.g. a proton) in your set-up
   -  Then make your detector bigger, to contain most of the resulting particles, the so-called hadronic shower
       
-  Visualize the shower for a few events
   -  Do they look similar?

-  Observe how the properties of shower changes
   - Between e- / e+ / gamma (i.e. electromagnetic showers) and pi- / pi+ / proton / neutron (i.e. hadronic showers)
             
   - Between different beam energies , e.g. 1 GeV , 10 GeV , 100 GeV
   - Between different physics lists, e.g. FTFP_BERT vs QGSP_BIC_HP

