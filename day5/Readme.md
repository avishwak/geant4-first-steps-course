Day 5 topics - Monday 31 May 2021
=================================

RECALL the weekend has intervened - more time to refresh needed !

Initial items
-------------
- Review homework & answer questions
- Follow-up concepts from homework - extended concepts ?
- Reset set-up of hands-on

New topics
----------
- Messengers - how to use them and to create one (if application is prepared)
- Next steps: multi-threading (idea / first try)
- Run physical setup (slab) and compare with data (=> motivate Advanced)

Followup topics & extention of existing concepts
------------------------------------------------
- More information about physics ?

Hands-on
--------
- 1. Messenger ( is it too complicated ? )
- 2. Run multi-threading
- 3. Installing Geant4 on your own system (optional?)

Homework - all optional to exercise and extend concepts
--------
...

Developers
----------
- Messengers - John / Mihaly
- Multi-threading / next steps - John + others ? 
- Comparing with physics
